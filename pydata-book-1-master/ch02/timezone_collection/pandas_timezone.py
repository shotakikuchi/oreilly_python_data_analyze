# pandasを使用したタイムゾーンの情報の集計

import matplotlib.pyplot as plt
import json
from pandas import DataFrame, Series
import pandas as pd
import numpy as np

path = "usagov_bitly_data.txt"

record = open(path).readline()
records = [json.loads(rec) for rec in open(path)]
frame = DataFrame(records)

# print(frame)


# frame['tz']という記法でタイムゾーンデータだけを抽出したシリーズオブジェクトを得ることができます。
# シリーズオブジェクトには value_counts というメソッドが用意されているので、これを使って集計結果の上位10位のタイム損を憑依させます。
tz_counts = frame['tz'].value_counts()

# print(tz_counts[:10])

# タイムゾーン情報が存在していない箇所には、fillnaメソッドを用いて'Missing'という文字列を埋めておく。
clean_tz = frame['tz'].fillna('Missing')
# print(clean_tz)

# 中身が空文字列である場合は'Unknown'という文字列に置き換える。
clean_tz[clean_tz == ''] = 'Unknown'
# print(clean_tz)

tz_counts2 = clean_tz.value_counts()
# print(tz_counts2[:10])

tz_counts2[:10].plot(kind='barh', rot=0)

plt.show()