import json
from collections import defaultdict
from collections import Counter

path = 'usagov_bitly_data.txt'
record = open(path).readline()

records = [json.loads(line) for line in open(path)]
# print(records[0]['tz'])

# これだとエラーになる
# time_zones = [rec['tz'] for rec in records]

# 条件を追加
time_zones = [rec['tz'] for rec in records if 'tz' in rec]

# 先頭の結果10位を表示
print(time_zones[:10])

# time_zoneの出現回数を調べる（Python標準関数を使う方法）
def get_counts(sequence):
  counts = {}
  for x in sequence:
    if x in counts:
      counts[x] += 1
    else:
      counts[x] = 1
  return counts

counts = get_counts(time_zones)

# print(counts['America/New_York'])

def get_counts2(sequence):
  counts = defaultdict(int)
  for x in sequence:
    counts[x] += 1
  return counts

len(time_zones)

# 上位10件のタイムゾーンが何であるかを調べる。
def top_counts(count_dict, n = 10):
  value_key_pairs = [(count, tz) for tz, count in count_dict.items()]
  value_key_pairs.sort()

  # sort()では昇順に並べ替えるため、-n として上位の結果として末尾から取り出すようにしている。
  return value_key_pairs[-n:]


print(top_counts(counts))

# 上位10件のタイムゾーンが何であるかを調べる。 Python標準ライブラリの collections.Counterを使うと、同じことをとても簡単に実現できる。
counts = Counter(time_zones)

# 上位10件を取得
print(counts.most_common(10))
