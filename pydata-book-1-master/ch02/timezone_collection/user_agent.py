import matplotlib.pyplot as plt
import json
from pandas import DataFrame, Series
import pandas as pd
import numpy as np

path = "usagov_bitly_data.txt"

records = [json.loads(rec) for rec in open(path)]
frame = DataFrame(records)
# print(records)

results = Series([x.split()[0] for x in frame.a.dropna()])
# print(results[:5])

# print(results.value_counts()[:8])

# ユーザーエージェントが存在していないレコードを除外
cframe = frame[frame.a.notnull()]

# それぞれのレコード文字列が'Windows'を含むかどうかを判定します。これには、NumPyの whereを使うことでできる。
operating_system = np.where(cframe['a'].str.contains('Windows'), 'Windows', 'Not Windows')
# print(operating_system[:5])

# それぞれのタイムゾーンと稼働OSの組で集計する。
by_tz_os = cframe.groupby(['tz', operating_system])

# 組ごとの集計はby_tz_os.size()で可能だが、見やすくするために pandas.DataFrameの提供する unstack() を用いて表形式で出力する。
agg_counts = by_tz_os.size().unstack().fillna(0)
# print(agg_counts)

# 昇順のソートを使用する
indexer = agg_counts.sum(1).argsort()

print(indexer[:10])

count_subset = agg_counts.take(indexer)[-10:]
print(count_subset)

# count_subset.plot(kind="barh", stacked=True)

normed_subset = count_subset.div(count_subset.sum(1), axis=0)
normed_subset.plot(kind="barh", stacked=True)
plt.show()
