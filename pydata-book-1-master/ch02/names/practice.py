import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

pieces = []
for year in range(1880, 2011):
  path = 'yob%d.txt' % year
  rec = pd.read_csv(path, names=['name', 'sex', 'births'])
  rec['year'] = year
  pieces.append(rec)

names = pd.concat(pieces, ignore_index=True)

# 年度別出生率
total_births = names.pivot_table('births', index='year', columns='sex', aggfunc=sum)
# print(total_births)

# 名前別出生率
total_names = names.pivot_table('births', index='name', aggfunc=sum)


# print(total_names)


def get_1000(group):
  return group.sort_index(by='births', ascending=False)[:1000]


def add_prop(group):
  births = group.births.astype(float)
  group['prop'] = births / births.sum()
  return group


names = names.groupby(['year', 'sex']).apply(add_prop)

top1000 = names.groupby(['year', 'sex']).apply(get_1000)
# print(top1000)


# 男子のtop1000
boys1000 = top1000[top1000.sex == 'M']
girls1000 = top1000[top1000.sex == 'F']


# 何番目のところで出生率の累計が50%に達するかを求める。
def cumsum_births(group, q=0.5):
  group = group.sort_index(by='births', ascending=False)
  return group.prop.cumsum().searchsorted(q) + 1


diversity = top1000.groupby(['year', 'sex']).apply(cumsum_births)
# print(diversity)
diversity = diversity.astype(float)
diversity = diversity.unstack('sex')
# print(diversity)
diversity.plot(title='Sum of prop per year')
plt.show()
