import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

names1880 = pd.read_csv('yob1880.txt', names=['name', 'sex', 'births'])
# print(names1880[:10])

# 性別別出生数を取得
names_by_sex_sum = names1880.groupby('sex').births.sum()
# print(names_by_sex_sum)

# ファイル群で提供される名前データを一つの巨大なデータフレームオブジェクトにまとめる。 これにはpanda.concatを使う。また新たな属性として年も追加。
years = range(1880, 2011)

pieces = []
columns = ['name', 'sex', 'births']

for year in years:
  path = 'yob%d.txt' % year
  frame = pd.read_csv(path, names=columns)

  # 新たな属性として年を追加
  frame['year'] = year
  pieces.append(frame)

# piecesに格納されたそれぞれの要素を、1つのデータフレームオブジェクトにまとめる。
names = pd.concat(pieces, ignore_index=True)
# print(names)

# groupby, あるいは pivot_tableを使えば、容易に年度ごとの男女別出生別数を得ることができます。
total_births = names.pivot_table('births', index='year', columns='sex', aggfunc=sum)


# print(total_births.tail())

# 続いてこのデータフレームオブジェクトに新たな列を追加する。propと言う名前で、全出生数に対するその名前の割合を求めてみましょう。
# 例えば、propが0.02であるということは、赤ちゃん100人がいたとしたら、その名前の赤ちゃんは2人いるということです。このpropを、年度別・性別ごとに計算していくことにします。
def add_prop(group):
  # integerどうしの徐さんは繰り上げ(floor)されてしまうのでfloatにキャスト
  births = group.births.astype(float)
  group['prop'] = births / births.sum()
  return group


names = names.groupby(['year', 'sex']).apply(add_prop)


# print(names)

# 年代・性別ごとの、上位1000件の名前がどのようなものであるか検証
def get_top1000(group):
  return group.sort_index(by='births', ascending=False)[:1000]


grouped = names.groupby(['year', 'sex'])
top1000 = grouped.apply(get_top1000)
# print(top1000)


# 上位1000件データを、男女別により分ける。
boys = top1000[top1000.sex == 'M']
girls = top1000[top1000.sex == 'F']

# pivot_tableを使って、top1000を年代別のデータとして整理し直してみる。
total_births = top1000.pivot_table('births', index='year', columns='name', aggfunc=sum)
# print(total_births)

# データフレームのplotメソッドを使って、幾つかの名前を病がする。John, Harry, Mary, Marilyn を選んでみます。
subset = total_births[['John', 'Harry', 'Mary', 'Marilyn']]
# print(subset)

# 描画
# subset.plot(subplots=True, figsize=(12, 10), grid=True, title='Number of births per year')
# plt.show()


# 多様化していく名付け
### 近年の人口増加に対して、Johnなどの名付けが減っていることが見て取れました。これには、子供の名前をつけるとき、いわゆる一般的な名前を選ばない傾向があるのではないかという仮説を考えることができます。
### これをデータから確認してみることにしましょうm。尺度の一つとして、その年の上位1,000県の名前が、その年の名前全体に対して占める割合を考えることができます。

table = top1000.pivot_table('prop', index='year', columns='sex', aggfunc=sum)
# table.plot(title='Sum of table1000.prop by year and sex', yticks=np.linspace(0, 1.2, 13), xticks=range(1880, 2010, 10))
# plt.show()


# 2010年の男子のデータ
boys2010 = boys[boys.year == 2010]

### これを降順にソートした上で、何番目の名前のところで50%に達するのかを調べます。
# NumPyには searchsortedという強力な配列関数があり、propの累積和を求め、次にこの累積和が0.5を超えるところがどこかを探します。
# この配列要素の探索にNumPyのsearchsortedを使うことができます。(累積はcumsum()を使う)

prop_cumsum = boys2010.sort_index(by='prop', ascending=False).prop.cumsum()
# print(prop_cumsum[:10])

# 何番目の名前のところで50%に達するのかを調べる
print(prop_cumsum.searchsorted(0.5))

# 同様に1990年のデータに対して調べる。
boys1990 = boys[boys.year == 1900]
prop_cumsum1900 = boys1990.sort_index(by='prop', ascending=False).prop.cumsum()
# print(prop_cumsum1990)
# 25番目で50%に達する。
print(prop_cumsum1900.searchsorted(0.5) + 1)


# 何番目の名前のところで50%に達するのかを調べる関数。
def get_quantifile_count(group, q=0.5):
  group = group.sort_index(by='prop', ascending=False)
  return group.prop.cumsum().searchsorted(q) + 1

### diversityに男女別の時系列データが格納する
diversity = top1000.groupby(['year', 'sex']).apply(get_quantifile_count)
# print(diversity)
diversity = diversity.unstack('sex')
# print(diversity.head())
diversity = diversity.astype(float)

diversity.plot(title='Number of popular names in top 50%')

plt.show()
