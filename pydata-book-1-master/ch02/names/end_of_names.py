import numpy as np

import pandas as pd
import matplotlib.pyplot as plt

pieces = []
for year in range(1880, 2011):
  path = "yob%d.txt" % year
  rec = pd.read_csv(path, names=['name', 'sex', 'births'])
  rec['year'] = year
  pieces.append(rec)

names = pd.concat(pieces, ignore_index=True)
# print(names)

get_last = lambda x: x[-1]
last_letters = names.name.map(get_last)
last_letters.name = 'last_letters'

tables = names.pivot_table('births', index=last_letters, columns=['sex', 'year'], aggfunc=sum)
# print(tables)


### 1880年から2010年までを見透す前にサンプルとしてそれぞれの期間の代表となる年度を3つ選びます。ここでは1910年、1960年、2010年としましょう。
### この3つの年度の集計数を、aからeまでの5文字について見てみると次のようになります。

subtable = tables.reindex(columns=[1910, 1960, 2010], level='year')
print(subtable.head())

# このままでは年度間での比較に的さない。このため、集計数を各年度の全出生数で割り、正規化しておきます。
letter_prop = subtable / subtable.sum().astype(float)


# print(letter_prop)

# fig, axes = plt.subplots(2, 1, figsize=(10, 8))
# letter_prop['M'].plot(kind='bar', rot=0, ax=axes[0], title="Male")
# letter_prop['F'].plot(kind='bar', rot=0, ax=axes[1], title="Female", legend=False)
# plt.show()

# 男の子の末尾文字「d」「n」「y」について先ほど同様に正規化して比較していきたいと思います。
# letter_prop_each_year = tables / tables.sum().astype(float)
# dny_ts = letter_prop_each_year.ix[['d', 'n', 'y'], 'M'].T
# # print(dny_ts.head())
#
# dny_ts.plot(style={'d': '-.', 'n': '-', 'y': ':'})
# plt.show()

### 男の子の名前として定着した女の子の名前（あるいはその逆）
# leslで始まる名前の一覧を計算する。

def get_1000(group):
  return group.sort_index(by='births', ascending=False)[:1000]


top1000 = names.groupby(['year', 'sex']).apply(get_1000)
all_names = top1000.name.unique()
mask = np.array(['lesl' in x.lower() for x in all_names])
lesley_like = all_names[mask]
# print(lesley_like)

### top1000からこれらleslの含まれる名前を抜き出し、この5種類の出現頻度を集計します。
filterd = top1000[top1000.name.isin(lesley_like)]

lesl_hindo = filterd.groupby('name').births.sum()
# print(filterd)


### 年代・性別順に集計
year_sex_sorted = filterd.pivot_table('births', index='year', columns='sex', aggfunc='sum')
year_sex_sorted = year_sex_sorted.div(year_sex_sorted.sum(1), axis=0)

# print(year_sex_sorted)
year_sex_sorted.plot(style={'M': 'k-', 'F': 'k--'})
plt.show()
