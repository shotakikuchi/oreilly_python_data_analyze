import pandas as pd

unames = ['user_id', 'gender', 'age', 'occupation', 'zip']
users = pd.read_table('users.dat', sep='::', header=None, names=unames, engine='python')

rnames = ['user_id', 'movie_id', 'rating', 'timestamp']
ratings = pd.read_table('ratings.dat', sep='::', header=None, names=rnames, engine='python')

mnames = ['user_id', 'title', 'genres']
movies = pd.read_table('movies.dat', sep='::', header=None, names=mnames, engine='python')

# print(users[:5])
# print(ratings[:5])
# print(movies[:5])


# 表の結合
data = pd.merge(pd.merge(ratings, users, ), movies)
# print(data[:5])


# ある映画について性別・年齢ごとの平均評価を出してみる。
mean_ratings = data.pivot_table('rating', index='title', columns='gender', aggfunc='mean')
# print(mean_ratings)


# 各映画タイトルで集計
ratings_by_title = data.groupby('title').size()
# print(ratings_by_title[:10])


# 250件以上のレビューがあったもののみを抽出
active_title = ratings_by_title.index[ratings_by_title >= 250]
# print(active_title)


# 250件以上のレビューがあった映画から性別・年齢ごとの平均評価を出してみる。
mean_ratings = mean_ratings.ix[active_title]
# print(mean_ratings)


# 得られたmean_ratingsから、女性評価の高いものの上位10権を見てみましょう。F(Female)列を軸に並び替えます。
top_female_ratings = mean_ratings.sort_index(by='F', ascending=False)
# print(top_female_ratings[:10])


# 男女間で評価の別れた映画の抽出
mean_ratings['diff'] = mean_ratings['M'] - mean_ratings['F']
sorted_by_diff = mean_ratings.sort_index(by='diff')
# print(sorted_by_diff[:15])

# Reverse order of rows, take first 15 rows
# print(sorted_by_diff[::-1][:15])


# タイトルごとの評価値の標準偏差の計算
rating_std_by_title = data.groupby('title')['rating'].std()
print(rating_std_by_title)

# 評価件数250件以上の映画のみを抽出（active_titlesとして計算済み）
rating_std_by_title = rating_std_by_title.ix[active_title]

# 映画タイトルごとの評価値の標準偏差の計算
rating_std_bytitle = data.groupby('title')['rating'].std()

# 評価件数250件以上の映画のみを抽出（active_titlesとして計算済み）
rating_std_by_title = rating_std_by_title.ix[active_title]

# 得られた rating_std_by_title（シリーズオブジェクト）を降順でソート
print(rating_std_by_title.order(ascending=False)[:10])
