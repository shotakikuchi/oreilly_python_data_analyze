import numpy as np

data = np.array([[0.9526, -0.246, -0.8856], [0.5639, 0.2379, 0.9104]])
print(data)

data = data * 10
print(data)

data = data + data

print(data.shape)  # (2, 3)
print(data.dtype)  # float64

